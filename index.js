// console.log("Hello World!");
let num = 2
let getCube = num ** 3;

console.log(`The cube of ${num} is ${getCube}`);
let address = [
	"258",
	"Washington Ave",
	"NW, California",
	"900111"
	]

let [houseNumber, street, state, postalCode] = address
console.log(`I live at ${houseNumber} ${street} ${state} ${postalCode}`);

let animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: 1075,
	weightUnit: "kgs",
	lengthFeet:20,
	lengthIn: 3 
}

let {name, species, weight, weightUnit, lengthFeet, lengthIn} = animal;

console.log(`${name} was a ${species}. He weighed at ${weight} ${weightUnit} with a measurement of ${lengthFeet} ft ${lengthIn} in.`);

let numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) =>{
	console.log(number);
})

const reduceNumber = numbers.reduce((total, currentValue) => total + currentValue,0);
console.log(reduceNumber);

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);
